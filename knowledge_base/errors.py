# An error raised when looking for a particular document that doesn't exist
class NoMatchingDocumentError(ValueError):
    pass


# An error raised when looking for a particular slide that doesn't exist
class NoMatchingSlideError(ValueError):
    pass


# An error raised when no pptx file found
class NoPresentationsFound(ValueError):
    pass