# Import of external modules
import re
import operator
from pptx import Presentation
from os import listdir, walk, makedirs
from os.path import isfile, join
import argparse

# Import of the internal modules
from knowledge_base.json_generation.utils import is_pptx, get_text_from_table, usefull_shape, sorted_nicely
from knowledge_base.errors import NoPresentationsFound

#  This function stores all documents and relevant info in a dictionnary
#
#  @param pipeline the preprocessing pipeline to use
#  @param path the path to the directory containing all the files
#  @return a dictionnary containing all the docs
def all_docs_to_json(pipeline, path):
	# Initializing the dictionnary that will contain elle the information
	all_docs = {}
	# we go through each document in  the directory, determine if they are a powerpoint, process them and add them to the json
	for (dirpath, _, filenames) in walk(path):
		for file in sorted_nicely(filenames):
			if is_pptx(file):
				doc_to_json(all_docs, join(dirpath, file), file, pipeline)
	if all_docs == {}:
		raise NoPresentationsFound('No pptx were foud. Please check your import path in the config.py file.')
	return all_docs


#  This function transforms a document into a json format and stores it into a dictionnary
#
#  @param all_docs the dictionnary to store the document
#  @param path the path to find the dictionnary
#  @param name the name of the file
#  @param pipeline a processing function
def doc_to_json(all_docs, path, name, pipeline):
	# we go through each slide of the document, store it as a json
	pr = Presentation(path)
	text = []
	slides = []
	n = len(pr.slides)
	for j in range(0, n):
		(new_text, new_slide) = slide_to_json(pr.slides[j], pipeline)
		text.extend(new_text)
		slides.append(new_slide)
	# we store the json of the document in the overall json
	all_docs['doc_'+str(len(all_docs))] = {"name": name,
										   "texte": text,
										   "number of slides": n,
										   "slides": slides}


#  This function transforms a slide into a json format
#
#  @param slide_object the object corresponding to the slide to analyse
#  @param pipeline the processing pipeline to use
#  @return a tuple containing the text of the slide and representation as a json of the slide
def slide_to_json(slide_object, pipeline):
	# we go through each shape of a json and store thel as a json
	n = len(slide_object.shapes)
	number_of_shapes = 0
	slide_text = []
	shapes = []
	for i in range(0, n):
		(new_text, new_shape) = shape_to_json(slide_object.shapes[i], pipeline)
		slide_text.extend(new_text)
		if new_shape['usefull']:
			shapes.append(new_shape)
			number_of_shapes += 1
	# we check if there are any comment associated to the slide and add them to the text of the slide as a shape
	if slide_object.has_notes_slide:
		comment = slide_object.notes_slide.notes_text_frame.text
		if comment != '':
			paragraphs = comment.split('\n')
			for j, _ in enumerate(paragraphs):
				paragraphs[j] = pipeline.process(paragraphs[j])
				slide_text.extend(paragraphs[j])
			new_shape = {"usefull": True,
						 "texte": slide_text,
						 "number of paragraphes": len(paragraphs),
						 "number of table": 0,
						 "paragraphes": paragraphs,
						 "table": []}
			shapes.append(new_shape)
			number_of_shapes += 1
	slide_json = {"texte": slide_text,
				  "number of shapes": number_of_shapes,
				  "shapes": shapes}
	return(slide_text, slide_json)


#  This function transforms a shape into a json format
#
#  @param shape_object the object corresponding to the shape to analyse
#  @param pipeline the processing pipeline to use
#  @return a tuple containing the text of the shape and representation as a json of the shape
def shape_to_json(shape_object, pipeline):
	# we check if the shape is useful and then process it with the get_info_of_shape function
	if usefull_shape(shape_object):
		text = []
		para_num = 0
		table_num = 0
		para = []
		table = []
		(text, para_num, table_num, para, table) = get_info_of_shape(
			shape_object, text, para_num, table_num, para, table, pipeline)
		return(text, {"usefull": True,
					  "texte": text,
					  "number of paragraphes": para_num,
					  "number of table": table_num,
					  "paragraphes": para,
					  "table": table})
	else:
		return('', {"usefull": False,
					"texte": [],
					"number of paragraphes": 0,
					"number of table": 0,
					"paragraphes": [],
					"table": []})


#  This function transforms a shape into a json format
#
#  @param shape_object the object corresponding to the shape to analyse
#  @param text the text of the shape to update
#  @param para_num the number of paragraphs of the shape to update
#  @param table_num the number of tables of the shape to update
#  @param para an array containing each paragraph of the shape to update
#  @param table an array containing each table of the shape to update
#  @param pipeline the processing pipeline to use
#
#  @return a tuple containing the text, the number of paragraphs, the number of tables, an array containing all paragraphs and an array containing all tables, updated
def get_info_of_shape(shape_object, text, para_num, table_num, para, table, pipeline):
	# if the shape is a non-empty text, we go through each paragraph, process them and store them into the para list as well as the text list
	if shape_object.has_text_frame and shape_object.text != '':
		for paragraph in shape_object.text_frame.paragraphs:
			if paragraph.text == '':
				continue
			x = pipeline.process(paragraph.text)
			para.append(x)
			para_num += 1
			text.extend(x)
	# if the shape is a table, we go through each cell, process them and store them into the table list as well as the text list
	elif shape_object.has_table:
		table_content = get_text_from_table(shape_object.table)
		for i, content in enumerate(table_content):
			table_content[i] = pipeline.process(content)
			table.extend(table_content[i])
			text.extend(table_content[i])
		table_num += 1
	# if the shape only contains other shape we process those shapes and add them into the json elements of the format
	else:
		if 'shapes' in str(dir(shape_object)):
			for s in shape_object.shapes:
				(text, para_num, table_num, para, table) = get_info_of_shape(
					s, text, para_num, table_num, para, table, pipeline)
	return (text, para_num, table_num, para, table)


#  This function generate the word_index dictionnary
#
#  @param json the json of all documents
#  @return a dictionnary linking words with their index in the matrix
def word_index_from_json(json):
	# we go through all the texte of each document and whenever we encounter a new word, we add a new entry in the dictionnary and we update the index number
	index = 0
	word_index = {}
	for doc in json.keys():
		for word in json[doc]["texte"]:
			if word not in word_index.keys():
				word_index[word] = index
				index += 1
	return word_index


#  This function creates a dictionnary linking stemmed words to the most recurrent word corresponding to the stem
#
#  @param pipeline the pipeline used to realize the processing
#  @param path the path to files
#
#  @return the dictionnary linking the stem and the words
def stem_word(pipeline, path):
	stm_wrd = {}
	for (dirpath, _, filenames) in walk(path):
		for file in sorted_nicely(filenames):
			if is_pptx(file):
				ppt = Presentation(join(dirpath, file))
				for slide in ppt.slides:
					if slide.has_notes_slide:
						comment = slide.notes_slide.notes_text_frame.text
						if comment != '':
							paragraphs = re.findall(r"[\w€]+", comment)
							for word in paragraphs:
								try:
									t_word = pipeline.process(word)[0]
									try:
										stm_wrd[t_word][word] += 1
									except:
										try:
											stm_wrd[t_word][word] = 1
										except:
											stm_wrd[t_word] = {}
											stm_wrd[t_word][word] = 1
								except:
									pass
					for shape in slide.shapes:
						stm_wrd = rec_stem_word(stm_wrd, pipeline, shape)

	for stem in stm_wrd:
		stm_wrd[stem] = max(stm_wrd[stem].items(),
							key=operator.itemgetter(1))[0]

	return stm_wrd


#  A function going through shapes to register the links between the stem and the word
#
#  @param stm_wrd a dictionnary linking stem and words
#  @param pipeline the pipeline used for the preprocessing
#  @param shape the shape to analyse
#
#  @return the links between stem and word for the shape
def rec_stem_word(stm_wrd, pipeline, shape):
	if shape.has_text_frame:
		text = shape.text
		if text != '':
			paragraphs = re.findall(r"[\w€]+", text)
			for word in paragraphs:
				try:
					t_word = pipeline.process(word)[0]
					try:
						stm_wrd[t_word][word] += 1
					except:
						try:
							stm_wrd[t_word][word] = 1
						except:
							stm_wrd[t_word] = {}
							stm_wrd[t_word][word] = 1
				except:
					pass
	elif shape.has_table:
		table_content = get_text_from_table(shape.table)
		for content in table_content:
			paragraphs = re.findall(r"[\w€]+", content)
			for word in paragraphs:
				try:
					t_word = pipeline.process(word)[0]
					try:
						stm_wrd[t_word][word] += 1
					except:
						try:
							stm_wrd[t_word][word] = 1
						except:
							stm_wrd[t_word] = {}
							stm_wrd[t_word][word] = 1
				except:
					pass
	else:
		if 'shapes' in str(dir(shape)):
			for s in shape.shapes:
				rec_stem_word(stm_wrd, pipeline, s)

	return stm_wrd
