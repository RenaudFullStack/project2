# Import of internal modules
from knowledge_base.json_generation.data_to_json import all_docs_to_json, word_index_from_json, stem_word
from knowledge_base.json_generation.processing.pipelines import pipelines

# Import of external modules
import os
import json


#  The function generating the jsons for a particular processing and storing them
#
#  @param args_pipeline the pipeline to use for the preprocessing
#  @param path_import the path to the documents
#  @param path_export the path where to store the various jsons
#  @params save a boolean set as True by default, True if we should save the generated json, False if not
def main_json_generation(args_pipeline, path_import, path_export, save=True):
	try:
		pipeline = pipelines[args_pipeline]()
	except:
		raise ValueError('There is no such pipeline as %s. See pipelines in the processing modules' % args_pipeline)

	all_docs = all_docs_to_json(pipeline, path_import)
	word_index = word_index_from_json(all_docs)
	stm_wrd = stem_word(pipeline, path_import)
	for key in stm_wrd.keys():
		if not key in word_index.keys():
			del stm_wrd[key]

	if not os.path.exists(os.path.join(path_export, args_pipeline)):
		try:
			os.makedirs(os.path.join(path_export, args_pipeline))
		except:
			raise ValueError('Check your export file in config.py as it is creating conflicts.')

	if save:
		with open(os.path.join(path_export, args_pipeline, args_pipeline+'.json'), "w") as f:
			json.dump(all_docs, f, indent=4, ensure_ascii=False)
		with open(os.path.join(path_export, args_pipeline, 'word_index.json'), "w") as f:
			json.dump(word_index, f, indent=4, ensure_ascii=False)
		with open(os.path.join(path_export, args_pipeline, 'processed_word_to_word.json'), "w") as f:
			json.dump(stm_wrd, f, indent=4, ensure_ascii=False)

	return all_docs, word_index, stm_wrd

