# Import of external modules
import re


#  This function sorts the given iterable in the way that humans expect
#
#  @param l an iterable
#  @return the iterable sorted
def sorted_nicely(l):
    # we define a function to convert potential digits into text
    def convert(text): return int(text) if text.isdigit() else text

    # we get the key for each element
    def alphanum_key(key): return [convert(c)
                                   for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)


#  This functions determines if the a file is a powerpoint (pptx file)
#
#  @param filename the name of the file
#  @return a boolean : True if the file is a pptx file, False if not
def is_pptx(filename):
    # we check the file name
    if filename[-5:] == '.pptx' and '$' not in filename:
        return True
    return False


#  This function returns the content of a pptx table row
#
#  @param table the pptx table
#  @param row_number the number of the row we want the content
#  @return a list of string
def get_row_of_table(table, row_number):
    # we check if the row exists
    if row_number >= len(table.rows):
        raise Exception('The table have less than {} rows.'.format(row_number))
    # we append the content of each row to a list that we return
    row_content = []
    for i in range(len(table.columns)):
        text_in_cell = table.cell(row_number, i).text
        row_content.append(text_in_cell)
    return row_content


#  This function retrieves the text from a table
#
#  @param shape a pttx table
#  @return an array of each cell of the table
def get_text_from_table(table):
    # we go through each row of the table and get the content of that row
    words = []
    for i in range(len(table.rows)):
        temp = get_row_of_table(table, i)
        for i in temp:
            if i != '':
                words.append(i)
    return words


#  This function is supposed to say if a shape is usefull or not.
#  It is usefull if it has text or a table in it or in one of its subshapes
#
#  @param shape a pptx shape
#  @return True or False
def usefull_shape(shape):
    # we check if the shape contains empty text
    if shape.has_text_frame and shape.text == '':
        return False
    # we check if the shape contains tables
    if shape.has_table:
        return True
    # we check if the shape contains sub shapes that are useful
    elif not shape.has_text_frame:
        try:
            for sub_shape in shape.shapes:
                if usefull_shape(sub_shape):
                    return True
        except:
            pass
        return False
    return True
