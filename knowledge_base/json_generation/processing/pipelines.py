# Import of the external modules
from nltk.stem.snowball import FrenchStemmer
from typing import List

# Import of the internal modules
from knowledge_base.json_generation.processing.processors import (Processor,
																  InitialsReplaceProcessor,
																  TokenizerProcessor,
																  LowercaseProcessor,
																  HourFormatProcessor,
																  UnitsProcessor,
																  IntTransformerProcessor,
																  StopWordsProcessor,
																  AccentsProcessor,
																  StemProcessor,
																  RemoveIntProcessor,
																  RemoveSmallWords,
																  ClearSpaces,
																  RemoveWordWithInt)


# A class concatenating several processors to define an overall action
class ProcessingPipeline:
	#  This function creates the Processing Pipelin
	#
	#  @param steps The different processors the pipelin is made off
	def __init__(self, steps: List[Processor]):
		self.steps = steps

	#  This function defines the process of the pipeline
	#
	#  @param x the object on which the pipeline will act
	def process(self, x):
		for step in self.steps:
			x = step.process(x)
		return x


#  More severe preprocessing
class Processing(ProcessingPipeline):
	def __init__(self):
		self.stop_words = ['de', 'la', 'le', 'les', 'l', 'd', 'a', 'des', 'et', 'en', 'du', 'pour', 'est', 'sur', 'un', 'par', 'ou', 'dans', 'au', 
							'une', 'i', 'ou', 'j', 'si', 'sont', 'que', 'avec', 'ne', 'n', 't', 'etre', 's', 'c', 'qui', 'on', 'non', 'se', 'aux', 
							'qu', 'ces', 'donc', 'cette', 'ce', 'il', 'pas', 'je']
		self.stop_words_stem = []
		self.min_len = 2
		self.stemmer = FrenchStemmer()
		super().__init__(steps=[
			TokenizerProcessor(),
			AccentsProcessor(),
			LowercaseProcessor(),
			RemoveWordWithInt(),
			StopWordsProcessor(self.stop_words),
			StemProcessor(self.stemmer),
			StemProcessor(self.stemmer),
			StemProcessor(self.stemmer),
			RemoveSmallWords(self.min_len),
			StopWordsProcessor(self.stop_words_stem),
			ClearSpaces()
		])

# The pipeline object to export
pipelines = {
	'Processing': Processing
}
