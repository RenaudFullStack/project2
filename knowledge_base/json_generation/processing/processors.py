# Import of the external modules
import re
import unidecode
from typing import List


# Import of the internal modules
from knowledge_base.json_generation.processing.utils_processing import is_int, is_hour, format_hour, gather_units, format_units, int_to_value


# An interface for classes with a process method
class Processor:
    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        raise NotImplementedError


# A class tokenizing a string
class TokenizerProcessor(Processor):
    #  This function creates the Tokenizer Processor
    #
    #  @param parser the tool used to parse the string
    def __init__(self, parser: str = r"[\w€]+"):
        self.parser = parser

    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        return re.findall(self.parser, x)


# A Processor to lower case all tokens
class LowercaseProcessor(Processor):
    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        for i, token in enumerate(x):
            x[i] = token.lower()
        return x


# A Processor uniformizing the format of hours to the following ..h.. format
class HourFormatProcessor(Processor):
    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        for i, token in enumerate(x):
            if is_hour(token):
                x[i] = format_hour(token)
        return x


# A Processor gathering numbers with their units and lowercasing the units
class UnitsProcessor(Processor):
    #  A function instanciating a UnitsProcessor object
    #
    #  @param units  a list of the potential units
    def __init__(self, units: List[str] = [], delete=False):
        self.units = units
        self.delete = delete

    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act

    def process(self, x, units=[]):
        x = gather_units(x, self.units)
        x = format_units(x, self.units)
        return x


# A Processor transforming digits to their string value
class IntTransformerProcessor(Processor):
    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        return int_to_value(x)


# A Processor returning the tokens without any accent
class AccentsProcessor(Processor):
    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        for i, token in enumerate(x):
            x[i] = unidecode.unidecode(token)
        return x


# A Processor to remove all stop words
class StopWordsProcessor(Processor):

    #  A function instanciating the StopWordsProcessor
    #
    #  @param stop_words a list of words to remove
    def __init__(self, stop_words: List[str] = []):
        self.stop_words = stop_words

    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        index = 0
        while index < len(x):
            if x[index] in self.stop_words:
                x.pop(index)
            else:
                index += 1
        return x


# A Processor stemming the tokens
class StemProcessor(Processor):
    #  A function instanciating the StemProcessor object
    #
    #  @param stemmer the stemmer used
    def __init__(self, stemmer=lambda token: token):
        self.stemmer = stemmer

    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        for i, token in enumerate(x):
            x[i] = self.stemmer.stem(token)
        return x


# A Processor replacing acronym by theur signfication
class InitialsReplaceProcessor(Processor):
    #  A function instanciating a InitialsReplaceProcessor
    #
    #  @param initials_dict the dictionnary to use to link acronyms with their significations
    #  @param parser the parser to use
    def __init__(self, initials_dict: dict = {}, parser: str = r"[\w€]+", keep=True):
        self.initials_dict = initials_dict
        self.parser = parser
        self.keep = keep

    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        replaced_token = []
        for token in x:
            if token in self.initials_dict.keys():
                if self.keep:
                    replaced_token.append(token)
                signification = self.initials_dict[token]
                replaced_token.extend(re.findall(self.parser, signification))
            else:
                replaced_token.append(token)
        return replaced_token


# A Processor removing tokens corresponding to an isolated number
class RemoveIntProcessor(Processor):

    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        index = 0
        while index < len(x):
            if is_int(x[index]):
                x.pop(index)
            else:
                index += 1
        return x


# A Processor to remove small tokens
class RemoveSmallWords(Processor):

    #  A function instanciating the RemoveSmallWords object
    #
    #  @param min_len the minimal lenght of a token
    def __init__(self, min_len):
        self.min_len = min_len

    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        index = 0
        while index < len(x):
            if len(x[index]) < self.min_len or x[index] == ' 1/2 ' or x[index] == ' 1/4 ':
                x.pop(index)
            else:
                index += 1
        return x


# A Processor that clears empty tokens
class ClearSpaces(Processor):
    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        for i, token in enumerate(x):
            x[i] = token.replace(' ', '')
        return x


# A Processor to remove words with an int inside
class RemoveWordWithInt(Processor):
    #  A function defining the action of the processor
    #
    #  @param x the object on which the processor will act
    def process(self, x):
        index = 0
        while index < len(x):
            if list(set('0123456789') & set(x[index])) != []:
                x.pop(index)
            else:
                index += 1
        return x
