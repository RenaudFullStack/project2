

#  A function determining determining if a string is an int
#
#  @param string the string to evaluate
#  @return True or False
def is_int(string):
    try:
        int(string)
        return True
    except:
        return False


#  A function determining if a string is an hour
#
#  @param string the string to evaluate
#  @return True or False
def is_hour(string):
    if len(string) == 3:
        return is_int(string[:2]) and (string[2] == "h" or string[2] == "H")
    elif len(string) == 2:
        return is_int(string[0]) and (string[1] == "h" or string[1] == "H")
    elif len(string) == 4:
        return (is_int(string[0]) and is_int(string[2:]) and (string[1] == "h" or string[1] == "H"))
    elif len(string) == 5:
        return (is_int(string[:2]) and is_int(string[3:]) and (string[2] == "h" or string[2] == "H"))


#  A function formating hours : 3h => 03H00
#
#  @param string the string to process
#  @return the formated hour
def format_hour(string):
    hour = string.lower()
    if len(hour) == 2:
        return "0" + hour + "00"
    elif len(hour) == 3:
        return hour + "00"
    elif len(hour) == 4:
        return "0" + hour
    else:
        return hour


#  A function gathering units : 
#
#  @param tokens the tokens to process
#  @param units the list of eventual units
#  @return the tokens formatted
def gather_units(tokens, units):
    i = 0
    while i < len(tokens)-1:
        if is_int(tokens[i]) and tokens[i+1].lower() in units:
            tokens[i] += tokens[i+1].lower()
            tokens.pop(i+1)
        i += 1
    return tokens


#  A function to format units
#
#  @param tokens the tokens to process
#  @param units the list of potential units
#  @return the tokens formatted
def format_units(tokens, units):
    for i, token in enumerate(tokens):
        for unit in units:
            if unit in token.lower() and is_int(token.lower().replace(unit, '')):
                tokens[i] = token.lower()
    return tokens


#  A function to replace digits by their value as a string
#
#  @param tokens the tokens to process
#  @return the tokens formatted
def int_to_value(tokens):
    integers = ["zéro", "un", "deux", "trois",
                "quatre", "cinq", "six", "sept", "huit", "neuf"]
    for i, token in enumerate(tokens):
        if is_int(token) and len(token) == 1:
            tokens[i] = integers[int(token)]
    return tokens
