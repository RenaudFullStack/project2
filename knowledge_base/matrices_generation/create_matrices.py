# Import of the exxternal modules
from time import time
import os
import numpy as np

# Import of the internal modules
from knowledge_base.matrices_generation.methods import word_word_methods
from knowledge_base.matrices_generation.utils_json import open_json
from knowledge_base.json_generation import main_json_generation


# This function checks if the run name already exists and ask for confirmation if they should change it
#
#  @param run_name the run_name to be checked
#  @param method the method to use
#  @param pipeline the processing pipeline to use
#  @param path_export the path to the directory where the fiules should be exported
#  @return the original run name if no conflict, else the new one
def check_if_exists(run_name, pipeline, path_export):
	if os.path.exists(os.path.join(path_export, pipeline, run_name)):
		print('PROCESS INTERRUPTED: this run_name already exists: %s.' % run_name)
		change = input('Enter a new run_name or press y to write over the old one: ')
		if change != 'y':
			run_name = change
		print('')
		return check_if_exists(run_name, pipeline, path_export)
	else:
		return run_name


#  This function builds the matrix for the different method
#
#  @param pipeline the pipeline to use
#  @param path_export the directory where the matrix are to be saved
#  @param run_name the name of the run
#  @param params a dictionanry storing the parameters for the different methods
#  @param to_run a dictionanry storing if the matrix need to be calculated for the different methods
#  @param test is True when you are in testing mode and you generate matrix to later test them, is False in deployment mode
#  @return a dictionnary storing the word_word_matrix for each method
def make_matrices(pipeline, path_import, path_export, run_name, params, to_run=True, test=True):

	word_word_matrices = {}
	print('')

	# If test, we try to load the json and the word_indexes, else we generate them 
	if test:
		# Check if this run will not overwrite anothoer one
		# If so, ask user to change the name of the run
		run_name = check_if_exists(run_name, pipeline, path_export)

		# Try to load the json containing data, else create it
		bool_1 = os.path.exists(os.path.join(path_export, pipeline, pipeline+'.json'))
		bool_2 = os.path.exists(os.path.join(path_export, pipeline, 'word_index.json'))
		print('Trying to load data...')
		if bool_1 and bool_2:
			json = open_json(os.path.join(path_export, pipeline, pipeline+'.json'))
			word_index = open_json(os.path.join(path_export, pipeline, 'word_index.json'))
			print('Sucess.\n')
		else:
			print('Data not found, generating again...')
			ti = time()
			json, word_index, _ = main_json_generation(pipeline, path_import, path_export, save=test)
			print('Done in %s s\n' % int(time()-ti))

	else:
		print('Generating json from data...')
		ti = time()
		json, word_index, _ = main_json_generation(pipeline, path_import, path_export, save=test)
		print('Done in %s s\n' % int(time()-ti))	



	# Calculate the word_word_matrix for each method
	for method in word_word_methods:
		if to_run[method]:
			print("Calculating %s word/word matrix..." % method)
			ti = time()
			word_word_matrices[method] = word_word_methods[method](path_export, pipeline, run_name, json, word_index, *params[method], save=test)
			print('Done in %s s\n' % int(time()-ti))

	return word_word_matrices

