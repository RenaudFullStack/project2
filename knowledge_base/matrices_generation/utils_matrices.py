# Import of external modules
import numpy as np
import json
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import pandas as pd
import math
from multiprocessing import Process, Pool
# Import of internal modules
from knowledge_base.matrices_generation.utils_json import retrieve
from functools import partial

#  This functions permutes the columns and rowws of the matrix to make it match the index used for each word
#
#  @param matrix the matrix to modify
#  @param traduction the link between the real and theoric index
#  @return the permuted matrixx
def translate_matrix_ind(matrix, traduction):
	permutation = np.zeros(matrix.shape)
	for i in range(len(traduction)):
		permutation[i, traduction[i]] = 1
	return np.dot(np.transpose(permutation), np.dot(matrix, permutation))


#  This function returns all the texte of a json joined with spaces
#
#  @param json the json containing all documents
#  @param level the level of detail we want to use
#  @return the text of a list of strings
def return_info_level(json, level):
	para = retrieve[level](json)
	return list(map(lambda x: ' '.join(x), para))


#  This functions allows to generate a matrix word/word out of a topic
#
#  @param topic a topic
#  @return the corresponding matrix
def topic_to_matrix(topic):
	a = np.mat(topic)
	b = np.transpose(a)
	matrix = b*a
	for i in range(len(topic)):
		if topic[i] < 0:
			for j in range(i, len(topic)):
				if topic[j] < 0:
					matrix[i, j] = 0
					matrix[j, i] = 0
	return matrix


#  This function creates an
#
#  @param docs the dictionnary of all documents
#  @param tf a boolean : True if we use TF (default value) and False if we use a TFIDF
#  @return the score of each word for each documents and a list of word allowing you to know which word corresponds to which matrix
def create_vectorizer(docs, tf=True):
	vectorizer = CountVectorizer() if tf else TfidfVectorizer()
	text_vec = vectorizer.fit_transform(docs)
	words = np.array(vectorizer.get_feature_names())
	return text_vec, words


#   This function applies a sigmoid over the data divided by a quantile
def uniform(quantiles, x):
	a = 1 / (1 + math.exp(-x/quantiles))
	return a

#  This function normalizes the database by dividing by the 0.9 quantile, and taking the sigmoid
#
#   @params matrix The matrix to process
#   @return the matrix normalized
def uniformizationProcess(vector):
	df = pd.DataFrame(data=vector)
	df = df[(df > 0.001)]
	quantile = df.quantile(0.9)[0]
	if quantile == 0:
		quantile = 0.00000001
	func = partial(uniform, quantile)
	return np.array(list(map(func, vector)))

#   This function applies the uniformization function to every row of the matrix
def uniformization_stand_by(matrix):
	p = Pool(4)
	return(np.array(p.map(uniformizationProcess, matrix)))

#   Another way to uniform because above it creates errors
def uniformization(matrix):
	quantile = np.percentile(matrix, 95)
	mini = np.min(matrix)
	func1 = lambda x: x + abs(mini)
	func1np = np.vectorize(func1)
	func = lambda x: 1 / (1 + math.exp(-x/quantile))
	funcnp = np.vectorize(func)
	return funcnp(func1np(matrix))
	# return matrix/np.max(matrix)
