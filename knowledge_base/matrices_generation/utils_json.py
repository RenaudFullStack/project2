# Import of the external modules
import re
import json

# Import of the internal modules
from knowledge_base.errors import NoMatchingDocumentError, NoMatchingSlideError


# This functions loads a json in a dictionnary
#
#  @param path the path of the json to load
#  @return the loaded json as a dictionnary
def open_json(path):
    with open(path, 'r') as f:
        return json.load(f)


''' Function to retrieve at document level'''

#  This function retrieves a document by its name
#
#  @param json the json containing all the documents
#  @param name the name of the document
#  @return the texte of that document as a string
def retrieve_a_doc_by_name(json, name):
    for key in json:
        if json[key]['name'] == name:
            return json[key]['texte']
    raise NoMatchingDocumentError('There is no document with that name')


#  This function retrieves all documents
#
#  @param json the json containing all the documents
#  @return a list of the texts of all documents as strings
def retrieve_all_docs(json):
    all_docs = []
    for key in json:
        all_docs.append(json[key]['texte'])
    return all_docs


''' Function to retrieve at slide level'''


#  This function retrieves all the slides of a document
#
#  @param json the json containing all the documents
#  @param doc_number the number of the document
#  @return a list of all the text of each slide of the document as strings
def retrieve_all_slides_of_a_doc(json, doc_number):
    if doc_number >= len(json) or doc_number < 0:
        raise NoMatchingDocumentError('There is no document with that number')
    all_slides = []
    doc = json['doc_'+str(doc_number)]
    n = doc['number of slides']
    for i in range(0, n):
        all_slides.append(doc['slides'][i]['texte'])
    return all_slides


#  This function retrieves all the slides of all documents
#
#  @param json the json containing all the documents
#  @return a list of all the text of each slide of all documents as strings
def retrieve_all_slides_of_all_docs(json):
    all_slides = []
    n = len(json)
    for i in range(0, n):
        all_slides.extend(retrieve_all_slides_of_a_doc(json, i))
    return all_slides


#  This function retrieves a particular slide of a document
#
#  @param json the json containing all the documents
#  @param doc_number the number of the document
#  @param slide_number the number of the slide
#  @return the text of the slide of the document as a string
def retrieve_a_slide(json, doc_number, slide_number):
    if doc_number >= len(json) or doc_number < 0:
        raise NoMatchingDocumentError('There is no document with that number')
    if slide_number >= json['doc_'+str(doc_number)]['number of slides'] or slide_number < 0:
        raise NoMatchingDocumentError('There is no slide with that number')
    return json['doc_'+str(doc_number)]['slides'][slide_number]['texte']


''' Function to retrieve at shape level'''


#  This function retrieves all the shapes of a slide
#
#  @param json the json containing all the documents
#  @param doc_number the number of the document
#  @param slide_number the number of the slide
#  @return a list of all the text of each shape of the slide as strings
def retrieve_all_shapes_of_a_slide(json, doc_number, slide_number):
    if doc_number >= len(json) or doc_number < 0:
        raise NoMatchingDocumentError('There is no document with that number')
    if slide_number >= json['doc_'+str(doc_number)]['number of slides'] or slide_number < 0:
        raise NoMatchingDocumentError('There is no slide with that number')
    slide = json['doc_'+str(doc_number)]['slides'][slide_number]
    result = []
    for j in range(0, slide['number of shapes']):
        result.append(slide['shapes'][j]['texte'])
    return result


#  This function retrieves all the shapes of a document
#
#  @param json the json containing all the documents
#  @param doc_number the number of the document
#  @return a list of all the text of each shape of the document as strings
def retrieve_all_shapes_of_a_doc(json, doc_number):
    if doc_number >= len(json) or doc_number < 0:
        raise NoMatchingDocumentError('There is no document with that number')
    result = []
    n = len(json['doc_'+str(doc_number)]['slides'])
    for i in range(0, n):
        result.extend(retrieve_all_shapes_of_a_slide(json, doc_number, i))
    return result


#  This function retrieves all the shapes of all documents
#
#  @param json the json containing all the documents
#  @return a list of all the text of each shape of all documents as strings
def retrieve_all_shapes_of_all_docs(json):
    result = []
    n = len(json)
    for i in range(0, n):
        result.extend(retrieve_all_shapes_of_a_doc(json, i))
    return result


""" Functions to retrieve at paragraph/table level """


#  This function retrieves all the paragraphs and tables of a slide
#
#  @param json the json containing all the documents
#  @param doc_number the number of the document
#  @param slide_number the number of the slide
#  @return a list of all the text of each paragraph/table of the slide as strings
def retrieve_all_paragraphe_of_a_slide(json, doc_number, slide_number):
    if doc_number >= len(json) or doc_number < 0:
        raise NoMatchingDocumentError('There is no document with that number')
    if slide_number >= json['doc_'+str(doc_number)]['number of slides'] or slide_number < 0:
        raise NoMatchingDocumentError('There is no slide with that number')
    slide = json['doc_'+str(doc_number)]['slides'][slide_number]
    result = []
    for j in range(0, slide['number of shapes']):
        shape = slide['shapes'][j]
        if shape['number of paragraphes'] > 0:
            result.extend(shape['paragraphes'])
        if shape['number of table'] > 0:
            result.append(shape['table'])
    return result


#  This function retrieves all the paragraphs and tables of a document
#
#  @param json the json containing all the documents
#  @param doc_number the number of the document
#  @return a list of all the text of each paragraph/table of the document as strings
def retrieve_all_paragraphe_of_a_doc(json, doc_number):
    if doc_number >= len(json) or doc_number < 0:
        raise NoMatchingDocumentError('There is no document with that number')
    result = []
    n = len(json['doc_'+str(doc_number)]['slides'])
    for i in range(0, n):
        result.extend(retrieve_all_paragraphe_of_a_slide(json, doc_number, i))
    return result


#  This function retrieves all the paragraphs and tables of all documents
#
#  @param json the json containing all the documents
#  @return a list of all the text of each paragraph/table of all documents as strings
def retrieve_all_paragraphe_of_all_docs(json):
    result = []
    n = len(json)
    for i in range(0, n):
        result.extend(retrieve_all_paragraphe_of_a_doc(json, i))
    return result


''' Exportable object'''

# A dictionnary associating the corresponding method to each scale of the documents
retrieve = {
    'slide': retrieve_all_slides_of_all_docs,
    'shape': retrieve_all_shapes_of_all_docs,
    'paragraph': retrieve_all_paragraphe_of_all_docs
}
