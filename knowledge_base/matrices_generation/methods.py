from knowledge_base.matrices_generation.NMF_matrix import main_NMF
from knowledge_base.matrices_generation.KMeans_matrix import main_KMeans

word_word_methods = {
    'NMF': main_NMF,
    'KMeans': main_KMeans
}