from knowledge_base.matrices_generation.utils_json import retrieve, open_json
from knowledge_base.matrices_generation.utils_matrices import translate_matrix_ind, return_info_level, topic_to_matrix, create_vectorizer
import os
import json
from sklearn.decomposition import NMF
import numpy as np
from knowledge_base.matrices_generation.utils_matrices import uniformization

#  This function creates the matrix corresponding to the NMF method and saves it
#
#  @param path_export the directory to save the matrix
#  @param preprocessor the preprocessing used
#  @param run_name the name of the run
#  @param n_topics the numbner of topics
#  @param tf a boolean True if we use a TF vectorizer, False if we use a TFIDF vectorizer
#  @param level which level of the documents we consider (documents, slide, shape, paragraph)
#  @param iteration the number of matrices we want to generate (then return the mean of all matrices)
#  @param save weather to save or not the matrix
#  @return the KMean matrix
def main_NMF(path_export, preprocessor, run_name, docs, word_index, n_topics, tf, level, iterations=1, save=True):
	# PROCESS DATA AT level
	docs_to_process = return_info_level(docs, level)
	# INITIATES CALCULATION
	text_vec, words = create_vectorizer(docs_to_process, tf=True)
	# GENERATE iter MATRICES AND CALCULATES THE MEAN VALUE
	raw_M = np.zeros((len(word_index.keys()), len(word_index.keys())))
	for _ in range(iterations):
		M = create_matrix(n_topics, text_vec, words)
		try:
			raw_M += M/iterations
		except:
			raise AssertionError('Fatal error: the vectorizer used here is making his own preprocessing and it does not match with your pipeline. Try another pipeline.')
	# REINDEX MATRIX TO FIT WORD_INDEX RELATION
	traduction = []
	for i in range(len(words)):
		traduction.append(word_index[words[i]])
	np.fill_diagonal(raw_M, 0)
	M_indexed = translate_matrix_ind(raw_M, traduction)
	M_indexed = uniformization(M_indexed)
	# SAVE THE MATRIX ALONG WITH RUN's PARAMETERS
	if save:
		if not os.path.exists(os.path.join(path_export, preprocessor, run_name, 'NMF_matrix')):
			try:
				os.makedirs(os.path.join(path_export, preprocessor, run_name, 'NMF_matrix'))
			except:
				raise ValueError('Check your export file in config.py as it is creating conflicts.')
		np.save(os.path.join(path_export, preprocessor, run_name, 'NMF_matrix', 'NMF_matrix.npy'), M_indexed)
		params = {
			'run name': run_name,
			'number of topics': n_topics,
			'tf or tfidf vectorizer': 'tf' if tf else 'tfidf',
			'level of lecture in pptx': level,
			'number of generations': iterations
		}
		json.dump(params, open(os.path.join(path_export, preprocessor, run_name, 'NMF_matrix', 'params.json'), 'w'), indent=4, ensure_ascii=False)
	return M_indexed
		
#  This functions compiles the matrix out of the word/word
#
#  @param n_topic the number of topics to find
#  @param text_vec the TF/TFIDF representation of the words
#  @param words a list to link text_vec index to words
#  @return the word_word matrix
def create_matrix(n_topics, text_vec, words):
	topicfinder = NMF(n_topics).fit(text_vec)
	topic_dists = topicfinder.components_
	topic_dists /= topic_dists.max(axis=1).reshape((-1, 1))

	matrix = np.zeros((len(words), len(words)))
	for topic in topic_dists:
		matrix += topic_to_matrix(topic)
	return matrix
