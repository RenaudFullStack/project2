# Knowledgebase generation module

This module will allow you to generate matrices that will give relations between words of your documents thanks to NLP algorithms.

Here we will explain the structure of the module

## Getting started

### Installing the environement 

#### Using pip-env 

To install the depedencies in a virtual environement using pipenv, you should execute the following command lines 

```
pip install pipenv
pipenv shell 
pipenv install
```

To install the depedencies using pipenv, you should execute the following command lines 

```
pip install pipenv
pipenv install
```

#### Using requirements 

To install dependencies using the requirements.txt file, you should execute the following command line:

```
pip install -r requirements.txt
```

### Complete the config.py file 

To use the the module you have to complete the config file with three fields:

* ``` path_import ```: the path to the directory containing all the pptx files you want to work on. Note that it will note take into account ppt files.
* ``` path_export ```: the path to the directory where to write all the generated objects (json, numpy objects) in order to later test their performances
* ``` path_export_app ```: the path to the directory where to write all the generated objects used to later build a database

You can use the template `config_example.py`

### Usage  

There are two ways to use the module:

#### Testing mode: to generate and save several models to later test them and evaluate their performances.

``` 
python run_test_generation.py 
``` 

You will have to set the parameters at the top of the script to parameterize your model generation:

* `pipeline`: the name of the preprocessing pipeleine you wish to use to generate models
* `run_name`: as you will generate many different models, give then a different name so you can find them later
* `to_run`: set the values to `False` if you don't want to create a model with this method for this run
* `params_method`: this is a dictionnary that contains the parameters of the model's generation. Check documentation to see the details of the parameters for each methods

#### Production mode: once you found the good parameters for each method, final generation of the word/word matrices that will be save in the `path_export_app` folder to later be used in database.

``` 
python run_save.py 
``` 

It will execute the complete process to save the elements in the back: generating a json from the powerpoints, compiling the different matrix out of it and storing them as numpy matrix and json elements. Just set these inputs in the `run_save.py` scipt:

* `pipeline`: the name of the preprocessing pipeleine you wish to use to generate models
* `params`: the best parameters you found for each method.


## Structure of the knowledge_base package 
The package is subdivided in four subpackage each one of them used to realize one of the major task of the module
At top level it also contains the errors.py file defining all the errors we might want to raise after.

### json_generation

This package handles the creation of a json containing all the documents as a list of words subdivided according to the hierachy of the powerpoint structure.
The resulting json has the following structure:
```
{
    doc_0:{
        name: the name of the documpent
        texte: the text of the document
        number of slide: the number of slide
        slides: [
            {
                texte: the text of the slide
                number of shapes: the number of shape of the slide
                shapes: [
                    {
                        useful: is the shape useful
                        texte: the text of the shape
                        number of paragraph: the number of paragraph of the shape
                        paragraph: [
                            [a list of the words of each paragraph],
                            [...],
                            ...
                        ]
                        number of table: the number of paragraph of the shape
                        table: a list of the words contained in the tables
                    },
                    {
                        ...
                    },
                    ...
                ]
            },
            {
                ...
            },
            ...
        ]
    },
    doc_1:{
        ...
    }
    etc.
}
```

The package contains three filesmain files are:
* ```data_to_json.py```: this file defines the methods going through the different level of the powerpoint structure and parsing it
* ```json_generation.py```: this file defines the overall method to call to generate the json

The package also contains a subpackage ```processing``` defining the different unit preprocessors and processing pipelines. Feel free to create your own units preprocessors and use tem to create your own pipeline.


### matrices_generation

This package allows to generate all the matrices and json necessary to constitution of the knowledge base word/word.

* ```create_matrices.py```: This file contains the methods allowing to create the various matrices 

The package contains a subpackage per method, each of them containing a file allowing to generate the word/word matrix storing the link between the words using the particular method.
