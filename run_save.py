# Import of internal modules
from knowledge_base import main_json_generation
from knowledge_base.matrices_generation.create_matrices import make_matrices
from knowledge_base.json_generation.processing.pipelines import pipelines as PIPELINES

# Import of external modules
from time import time
import sys
import os
import json
import numpy as np
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

# Environment
from config import path_export_app, path_import, path_export

params = {
	'NMF': [20, True, 'shape', 1],
	'KMeans': [40, True, 'slide', 1]
}
to_run = {x: True for x in params.keys()}

if __name__ == "__main__":

	# creating the export_data fileto export the data if it doesn't already exist
	if not os.path.exists(os.path.join(path_export_app)):
		try:
			os.makedirs(os.path.join(path_export_app))
		except:
			raise ValueError('Check your export file in config.py as it is creating conflicts.')

	print('\nAvailable pipelines :')
	print(PIPELINES.keys())
	pipeline = input('Which pipeline do you which to use ? ')

	# Ask verification for parameters
	print('\nThe word_word database is about to be calculated with these parameters :')
	print(params)
	stop = input('Press enter to validate, or any other key to stop. You can set other parameters in the script. ')

	if len(stop) > 0:
		print('Ending.')
	else:
		# generating the word_word graph in the database
		matrices = make_matrices(pipeline, path_import, path_export_app, '', params, to_run, test=False)
		# save word_word_matrix and dic_method
		np.save(os.path.join(path_export_app, pipeline, 'word_word_matrix.npy'), matrices)
