from config import path_export, path_import
from knowledge_base import main_json_generation
from knowledge_base.matrices_generation.create_matrices import make_matrices
from time import time
import os
import json
import numpy as np
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)


# Environment
from config import path_export, path_import 

if __name__ == "__main__":

	# PARAMETRING THE RUN
	pipeline = 'Processing'
	run_name = 'test_1'

	# Which method for this run
	to_run = {
		'NMF': True,
		'KMeans': True
	}

	# See detail of params for each method in doc
	params = {
		'NMF': [20, True, 'slide', 1],
		'KMeans': [20, True, 'slide', 1]
	}

	# No return, matrices are saves to later be tested
	make_matrices(pipeline, path_import, path_export, run_name, params, to_run)



